We install conventional & geothermal equipment for new construction or replacement upgrades. Excellent service technicians & high quality duct work, with years of experience. Amana & Water Furnace dealer. Fully licensed & insured. Family business established in 1968 by Herman & Cordelia Peters.

Address: 5644 State Highway 25, Cape Girardeau, MO 63701, USA

Phone: 573-243-6282

Website: http://www.petersheatingandair.biz
